package pro.izcali.p02.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import pro.izcali.p02.R;
import pro.izcali.p02.objects.Chat;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder>{

    Context mContext;
    List<Chat> lsChats;

    public ChatsAdapter(Context mContext, List<Chat> lsChats) {
        this.mContext = mContext;
        this.lsChats = lsChats;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.card_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        if(lsChats.get(i).getVisto()){
            viewHolder.ivpalomas.setImageResource(R.drawable.ic_readed);
        }else{
            viewHolder.ivpalomas.setImageResource(R.drawable.ic_sended);
        }

        viewHolder.tvmensaje.setText(lsChats.get(i).getMensaje());
        viewHolder.tvhora.setText(lsChats.get(i).getHora());
        viewHolder.tvnombre.setText(lsChats.get(i).getNombre());

        Glide.with(mContext)
                .load(lsChats.get(i).getPerfil())
                .apply(RequestOptions.circleCropTransform())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        viewHolder.ivperfil.setImageResource(R.drawable.ic_sended);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(viewHolder.ivperfil);



    }

    @Override
    public int getItemCount() {
        return lsChats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvnombre, tvmensaje, tvhora;
        ImageView ivperfil, ivpalomas;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvnombre = itemView.findViewById(R.id.tvNombre);
            tvhora = itemView.findViewById(R.id.tvHora);
            tvmensaje = itemView.findViewById(R.id.tvMensaje);
            ivperfil = itemView.findViewById(R.id.ivPerfil);
            ivpalomas = itemView.findViewById(R.id.ivVista);
        }
    }
}
