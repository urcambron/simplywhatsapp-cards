package pro.izcali.p02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import pro.izcali.p02.adapters.ChatsAdapter;
import pro.izcali.p02.objects.Chat;

public class Base3 extends AppCompatActivity {
    RecyclerView rvChats;
    List<Chat> lschats;
    ChatsAdapter rvAdapter;

    GridLayoutManager gridLayoutManager;

    DatabaseReference refChats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base3);

        rvChats = findViewById(R.id.rvWhats);

        lschats = new ArrayList<>();
        initRecyclerView();

        initFirebase();

    }

    private void initFirebase() {
        refChats = FirebaseDatabase.getInstance().getReference("chats");
        refChats.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lschats.clear();
                for (DataSnapshot chat:dataSnapshot.getChildren()){
                       lschats.add(chat.getValue(Chat.class));

                }

                rvAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initRecyclerView() {

        gridLayoutManager = new GridLayoutManager(this, 1);
        rvChats.setLayoutManager(gridLayoutManager);

        rvAdapter = new ChatsAdapter(this, lschats);
        rvChats.setAdapter(rvAdapter);

    }
}
