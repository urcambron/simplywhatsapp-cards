package pro.izcali.p02;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main2Activity extends AppCompatActivity {
    TextView tvbase;
    EditText edinput;

    FirebaseDatabase fbdata;
    DatabaseReference myref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        /*tvbase = findViewById(R.id.tvEntrada);
        edinput = findViewById(R.id.edAdd);

        fbdata = FirebaseDatabase.getInstance();
        myref = fbdata.getReference("/dato/valor");

        myref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String texto = dataSnapshot.getValue(String.class);
                tvbase.setText(texto);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        edinput.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                sendToFirebase();
                return false;
            }
        });*/

        startActivity(new Intent(this, Base3.class));


    }

    private void sendToFirebase() {
        String act = edinput.getText().toString();

        if(!act.isEmpty()){
            myref.setValue(act);
        }
    }
}
