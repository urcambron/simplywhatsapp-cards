package pro.izcali.p02;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity {

    EditText edcorreo, edpass;
    Button btLogin;

    ProgressBar pg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseUser dummy = FirebaseAuth.getInstance().getCurrentUser();

        if(dummy == null){
            edcorreo = findViewById(R.id.edCorreo);
            edpass = findViewById(R.id.edPassword);
            btLogin = findViewById(R.id.btLogin);

            pg = findViewById(R.id.pgbar);
            pg.setVisibility(View.GONE);

            btLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iniciarSesion();
                }
            });
        }else{
            startActivity(new Intent(getApplicationContext(), Main2Activity.class));
        }


    }

    private void iniciarSesion() {

        String pass = edpass.getText().toString();
        String correo = edcorreo.getText().toString();

        if(!correo.isEmpty()){
            if(!pass.isEmpty()){
                pg.setVisibility(View.VISIBLE);

                FirebaseAuth mauth = FirebaseAuth.getInstance();

                mauth.signInWithEmailAndPassword(correo, pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), Main2Activity.class));
                        }else{
                            Toast.makeText(MainActivity.this, "Error al iniciar sesión", Toast.LENGTH_SHORT).show();
                            pg.setVisibility(View.GONE);
                        }
                    }
                });


            }else{
                edpass.setError("Ingresa password");
            }

        }else{
            edcorreo.setError("Ingresa un correo");
        }
    }
}
