package pro.izcali.p02.objects;

public class Chat {

    String perfil, nombre, mensaje, hora;
    Boolean visto;

    public Chat(String perfil, String nombre, String mensaje, String hora, Boolean visto) {
        this.perfil = perfil;
        this.nombre = nombre;
        this.mensaje = mensaje;
        this.hora = hora;
        this.visto = visto;
    }

    public Chat() {
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Boolean getVisto() {
        return visto;
    }

    public void setVisto(Boolean visto) {
        this.visto = visto;
    }
}
